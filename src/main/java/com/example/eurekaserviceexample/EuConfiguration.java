package com.example.eurekaserviceexample;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EuConfiguration {

    @Value("${spring.cloud.client.hostname}")
    private String hostname;


    public String getHostname() {
        return hostname;
    }
}
