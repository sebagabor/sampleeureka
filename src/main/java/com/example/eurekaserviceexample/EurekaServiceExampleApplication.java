package com.example.eurekaserviceexample;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.Bean;

import java.nio.file.Files;
import java.nio.file.Paths;

@EnableEurekaServer
@SpringBootApplication
public class EurekaServiceExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaServiceExampleApplication.class, args);
    }


    @Bean
    public CommandLineRunner run(EuConfiguration euConfiguration) {
        return strings -> {

            System.out.println(Paths.get(".").toAbsolutePath().toString());
            System.out.println(euConfiguration.getHostname());


//            Files.lines()

//            Files.walkFileTree()


        };
    }
}
